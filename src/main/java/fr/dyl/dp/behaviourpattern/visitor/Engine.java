package fr.dyl.dp.behaviourpattern.visitor;

public class Engine implements CarElement {

    public void accept(CarElementVisitor visitor) {
        visitor.visit(this);
    }
}
