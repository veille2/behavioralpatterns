package fr.dyl.dp.behaviourpattern.visitor;

public class VisitorDemo {

    static public void main(String[] args) {
        var car = new Car();

        var printVisitor = new CarElementPrintVisitor();
        var doVisitor = new CarElementDoVisitor();

        printVisitor.visitCar(car);
        doVisitor.visitCar(car);
    }
}
