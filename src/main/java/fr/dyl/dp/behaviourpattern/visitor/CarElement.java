package fr.dyl.dp.behaviourpattern.visitor;

public interface CarElement {

    void accept(CarElementVisitor visitor);
}
