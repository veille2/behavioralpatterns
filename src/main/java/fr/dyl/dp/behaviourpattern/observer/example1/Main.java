package fr.dyl.dp.behaviourpattern.observer.example1;


class Main {
    public static void main(String args[]) {

        var averageScoreDisplay = new AverageScoreDisplay();
        var currentScoreDisplay = new CurrentScoreDisplay();

        var cricketData = new CricketData();

        cricketData.registerObserver(averageScoreDisplay);
        cricketData.registerObserver(currentScoreDisplay);

        cricketData.dataChanged();

        cricketData.unregisterObserver(averageScoreDisplay);

        cricketData.dataChanged();
    }
}
