package fr.dyl.dp.behaviourpattern.observer.example2;

public interface Subject {

    void registerObserver(Observer observer);

    void unregisterObserver(Observer observer);

    void addMark(float mark);
}
