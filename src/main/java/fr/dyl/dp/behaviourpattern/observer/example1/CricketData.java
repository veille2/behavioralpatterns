package fr.dyl.dp.behaviourpattern.observer.example1;

import java.util.ArrayList;
import java.util.List;

class CricketData implements Subject {

    private int runs;
    private int wickets;
    private float overs;
    private List<Observer> observerList;

    public CricketData() {
        observerList = new ArrayList<>();
    }

    private int getLatestRuns() {
        return 90;
    }

    private int getLatestWickets() {
        return 2;
    }

    private float getLatestOvers() {
        return (float) 10.2;
    }

    @Override
    public void registerObserver(Observer o) {
        observerList.add(o);
    }

    @Override
    public void unregisterObserver(Observer o) {
        observerList.remove(observerList.indexOf(o));
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observerList) {
            observer.update(runs, wickets, overs);
        }
    }

    public void dataChanged() {
        runs = getLatestRuns();
        wickets = getLatestWickets();
        overs = getLatestOvers();

        notifyObservers();
    }
}