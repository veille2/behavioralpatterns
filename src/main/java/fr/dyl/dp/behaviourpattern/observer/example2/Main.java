package fr.dyl.dp.behaviourpattern.observer.example2;


class Main {
    public static void main(String args[]) {

        var student = new Student();
         new AverageObserver(student);

        student.addMark(16);
        student.addMark(12);

        System.out.println(student.getAverage());
    }
}
