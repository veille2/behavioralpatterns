package fr.dyl.dp.behaviourpattern.observer.example2;

public class AverageObserver extends Observer {

    public AverageObserver(Student student) {
        this.student = student;
        this.student.registerObserver(this);
    }

    @Override
    void update() {
        float average = 0;

        for (float note : student.getMarks()) {
            average += note;
        }

        average /= student.getMarks().size();

        student.setAverage(average);
    }
}
