package fr.dyl.dp.behaviourpattern.observer.example2;

public abstract class Observer {

    protected Student student;

    abstract void update();
}
