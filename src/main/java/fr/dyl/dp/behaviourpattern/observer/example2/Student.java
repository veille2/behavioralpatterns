package fr.dyl.dp.behaviourpattern.observer.example2;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Student implements Subject {

    private List<Observer> observers;
    private List<Float> marks;
    private float average;

    public Student() {
        observers = new ArrayList<>();
        marks = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void unregisterObserver(Observer o) {
        observers.remove(observers.indexOf(o));
    }

    @Override
    public void addMark(float mark) {
        marks.add(mark);
        notifyObservers();
    }

    private void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
