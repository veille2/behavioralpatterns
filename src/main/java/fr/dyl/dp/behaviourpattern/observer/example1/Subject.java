package fr.dyl.dp.behaviourpattern.observer.example1;

public interface Subject {

    void registerObserver(Observer observer);

    void unregisterObserver(Observer observer);

    void notifyObservers();
}
