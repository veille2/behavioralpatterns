package fr.dyl.dp.behaviourpattern.observer.example1;

public interface Observer {

    void update(int runs, int wickets, float overs);
}
