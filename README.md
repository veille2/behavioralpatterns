Observer pattern
It defines a one-to-many dependency between objects so that when one object changes state, all of its dependents are notified and updated automatically.

The object which is being watched is called the subject. The objects which are watching the state changes are called observers or listeners.

We use it when we have to manage events.

example

The observer pattern is very common in Java. For example, you can define a listener for a button in a user interface. If the button is selected, the listener is notified and performs a certain action.

How to do : 
In a class that must trigger events, we add:

As an attribute: a list of Observers
A method allowing to add an Observer in the list
A method allowing to send a signal to all its observers.
"Observer" is an abstract class with a signal method, inherited from "concrete" observers that implement this method.

When the state of the class changes it must send a signal to all its observers who must take the necessary action according to the new state of the class.



Visitor: 

It separates algorithms and the objects they operate on.
visitor is an interface defining new operations on another class.

The Visitor Pattern makes it possible to separate data and the associated processing for this data. This design pattern allows an outer class to be aware of the exact type of instances of a set of classes.

In practice, the visitor design model is implemented as follows: each class that can be “visited” must provide a public “accept” method taking as argument an object of the “visitor” type. The “accept” method will call the “visit” method of the “visitor” type object with the visited object as an argument. In this way, a visitor object will be able to know the reference of the visited object and call its public methods to obtain the data necessary for the processing to be performed (calculation, display, etc.).

Ref: 

[observer-pattern]

[observer-pattern]: https://www.geeksforgeeks.org/observer-pattern-set-2-implementation/